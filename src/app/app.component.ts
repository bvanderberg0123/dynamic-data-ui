import { Component } from '@angular/core';
import { RestApiService } from './dynamic-controls/services/rest-api.service';

@Component({
  selector: 'app-root',
  template: `
      <mat-card>
        <mat-card-actions>
          <button mat-raised-button (click)="getValues()">Get Data</button>
        </mat-card-actions>
        <span class="mat-h1">Config Renderer</span>
        <mat-card-content>
          <app-dynamic-form *ngIf="data" [data]="data"></app-dynamic-form>
        </mat-card-content>
      </mat-card>
  `
})
export class AppComponent {
  data: any;

  constructor(public restApi: RestApiService) {}

  getValues(){
    this.restApi.get().subscribe((res: {})=> {
      console.log("Res:", res);
      // this.data = new Array(res[0]);
      this.data = new Array(
        {
          id : 1,
          name : "Test1",
          randomArr : [
            {
              id : 2,
              name : "Test 2"
            },

          ],
          objectTest : {
            testfield : "Test3"
          }
        }
      );
    });
  }
}
