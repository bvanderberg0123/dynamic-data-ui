import { NgModule } from '@angular/core';

import {MatButtonModule, MatCheckboxModule, MatInputModule, MatFormFieldModule, MatCardModule} from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatInputModule, MatFormFieldModule, MatCardModule],
  exports: [MatButtonModule, MatCheckboxModule, MatInputModule, MatFormFieldModule, MatCardModule],
})
export class MaterialModule { }