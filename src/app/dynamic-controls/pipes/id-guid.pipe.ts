import { Pipe, PipeTransform } from '@angular/core';
import { Guid } from '../utilities/guid.utility';

@Pipe({ name: 'idguid' })
export class IdGuidPipe implements PipeTransform {
  transform(id : string) : string {
      return id + "__" +Guid.newGuid();
  }
}