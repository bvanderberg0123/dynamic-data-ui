import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ControlBase } from '../controls/control-base';
import { ControlService } from '../services/control.service';

@Component({
    selector: 'app-dynamic-form',
    templateUrl: '../views/dynamic-form.component.html',
    providers: [ControlService]
})
export class DynamicFormComponent implements OnInit {

    @Input() data: any;
    controls: ControlBase<any>[] = [];
    form: FormGroup;
    payLoad = '';

    constructor(private cs: ControlService) { }

    ngOnInit() {
        var res = this.cs.toFormGroup(this.data);
        this.form = res.formGroup;
        this.controls = res.controls;
    }

    onSubmit() {
        console.log(this.form.value);
        this.payLoad = JSON.stringify(this.form.value);
    }
}