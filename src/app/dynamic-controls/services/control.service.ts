import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, AbstractControl } from '@angular/forms';

import { ControlBase } from '../controls/control-base';
import { TextboxControl } from '../controls/textbox-control';
import { CheckboxControl } from '../controls/checkbox-control';

@Injectable()
export class ControlService {
  constructor() { }

  controls: ControlBase<any>[] = [];

  toFormGroup(data: any) {
    var resultGroup: FormGroup;
    data.forEach(item => {
      resultGroup = this.toFormGroupOfFields(item);
    });

    var result = {
      formGroup: resultGroup,
      controls: this.controls
    }
    return result;
  }

  toFormGroupArray(data: any): AbstractControl[] {
    var result = [];
    data.forEach(element => {
      result.push(this.toFormGroupOfFields(element));
    });
    return result;
  }

  toFormGroupOfFields(data: any): FormGroup {
    var result = {};
    Object.keys(data).forEach(key => {
      switch (typeof data[key]) {
        case "string":
          this.determineInput(key, data[key]);
          result[key] = new FormControl({ value : data[key], disabled : key.toLowerCase().includes("id") ? true : false})
          break;
        case "boolean":
          this.createCheckBox(key, data[key]);
          result[key] = new FormControl({ value : data[key], disabled : key.toLowerCase().includes("id") ? true : false})
          break;
        case "number":
          this.createTextBox(key, data[key]);
          result[key] = new FormControl({ value : data[key], disabled : key.toLowerCase().includes("id") ? true : false})
          break;
        case "object":
          if (Array.isArray(data[key])) {
            result[key] = new FormArray(this.toFormGroupArray(data[key]))
          } else {
            result[key] = this.toFormGroupOfFields(data[key]);
          }
          break;
        case "undefined":
          this.createTextBox(key, "undefined", true);
          result[key] = new FormControl({ value: data[key], disabled: true })
          break;
      }
    });
    return new FormGroup(result)
  }

  determineInput(key: string, value: string, disabled: boolean = false) {
    if (value.match("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$")) {
      //Is a URL
      //TODO: Create HREF logic
      this.createTextBox(key, value);
      //TODO: write logic to render image
      //TODO: write logic to create textarea 
    } else {
      this.createTextBox(key, value);
    }
  }

  createTextBox(key: string, value: string, disabled: boolean = false) {
    disabled = key.toLowerCase().includes("id") ? true : false;
    this.controls.push(new TextboxControl({
      key: key,
      label: key,
      value: value,
      required: true,
      disabled: disabled
    }));
  }

  createCheckBox(key: string, value: boolean, disabled: boolean = false) {
    this.controls.push(new CheckboxControl({
      key: key,
      label: key,
      value: value,
      required: true,
      disabled: disabled
    }));
  }

}